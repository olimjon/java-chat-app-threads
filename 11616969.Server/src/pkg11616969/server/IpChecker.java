/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;

/**
 *
 * @author Helkern
 */
public class IpChecker {

    public static String getIp() throws Exception { // Getting local and external ip addresses
        String external;
        InetAddress local = InetAddress.getLocalHost(); // local ip
        URL whatismyipAPI = new URL("http://checkip.amazonaws.com"); // using amazons api to get current ip
        BufferedReader bf = null;
        try {
            bf = new BufferedReader(new InputStreamReader(
                    whatismyipAPI.openStream())); //Opens a connection to this URL and returns an InputStream for reading from that connection and attaches it to bufferreader.
            external = bf.readLine(); //reads and stores into string var
        } catch (Exception e) {
            external = "Could not connect to the api server";
        } finally { // on finish close all buffer reader
            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "Local ip address: " + local + "\n External ip address: " + external;
    }
}

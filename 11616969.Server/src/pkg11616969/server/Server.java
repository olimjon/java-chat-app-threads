/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969.server;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 *
 * @author Helkern
 */
public class Server {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Thread starter = new Thread(new ServerStart()); //New instance of the ServerStart class and running it in thread, allows to have multiple connections
        starter.start();

        System.out.println("Starting the host...\n");
        try {// Displaying host address details
            System.out.println(IpChecker.getIp() + "\n");
            System.out.println("The host is started");
        } catch (UnknownHostException e) {
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static ArrayList clientOutputStreams;
    static ArrayList<String> users;
    static HashMap<String, String> clientsHashMap = new HashMap<String, String>();

    ; 

    public static class ClientHandler implements Runnable { // class with runnable interface

        private BufferedReader _reader; //for reading from inputstream
        private Socket _clientSocket; //to get client socket
        private PrintWriter _client;

        public ClientHandler(Socket clientSocket, PrintWriter user) { //constructor to handle client requests
            _client = user;
            try {
                _clientSocket = clientSocket;
                _reader = new BufferedReader(new InputStreamReader(_clientSocket.getInputStream()));
            } catch (Exception ex) {
                System.out.println("Unexpected error happened... \n");
            }

        }

        @Override
        public void run() {
            String message;
            final String connect = "Connect", disconnect = "Disconnect", chat = "Chat"; //final string for matching and checking incoming data
            String[] incomingData;

            try {
                while ((message = _reader.readLine()) != null) { // while incomeing data exists
                    incomingData = message.split(":"); // splitting incoming data string into array by : sign
                    clientsHashMap.put(String.valueOf(_client.hashCode()), incomingData[0]); // rewriting hashmap value for this specific printwriter user(key)
                    if (incomingData[2].equals(connect)) {//if connection, then display message on server and send message to all clients
                        System.out.println("User [" + incomingData[0] + "] has connected.");
                        sendMessage((incomingData[0] + ":" + incomingData[1] + ":" + chat)); //ex. Nick:has connected.:Chat
                        userAdd(incomingData[0]);//add the user
                    } else if (incomingData[2].equals(disconnect)) {
                        System.out.println("User [" + incomingData[0] + "] has disconnected.");
                        sendMessage((incomingData[0] + ":has disconnected." + ":" + chat));//ex. Nick:has disconnected.:Chat
                        userRemove(incomingData[0]);//remove the user when he clicks the disconnect button
                    } else if (incomingData[2].equals(chat)) {
                        System.out.println("User [" + incomingData[0] + "] sent a message: '" + incomingData[1] + "'");
                        sendMessage(message);//ex. Nick:Hello.:Chat the client side will decypher that
                    } else {
                        System.out.println("No Conditions were met. \n");
                    }
                }
            } catch (Exception ex) {
                String c = String.valueOf(_client.hashCode()); //current user printwriter hashcode as an id
                System.out.println("User [" + clientsHashMap.get(c) + "] has lost a connection"); // displaying info if user lost connection with the server
                users.remove(clientsHashMap.get(c));//removing the user from the users arraylist
                clientOutputStreams.remove(_client);//removing printwriter from the arraylist
            }
        }
    }

    public static class ServerStart implements Runnable { // this class is going to be used by thread, thus must use runnable interface

        @Override
        public void run() {
            clientOutputStreams = new ArrayList(); // init new arrays instances
            users = new ArrayList();

            try {
                ServerSocket serverSocket = new ServerSocket(2017); // Creating a server socket and bounding to the 2017 port.

                while (true) {
                    Socket clientSocket = serverSocket.accept(); // waiting for clients to join and accepting any sending requests
                    PrintWriter printWriter = new PrintWriter(clientSocket.getOutputStream()); //getting outputstream from client side and Printing formatted representations of objects to a text-output stream.
                    clientOutputStreams.add(printWriter); // adding the printwriter, (specific user) to the arraylist

                    clientsHashMap.put(String.valueOf(printWriter.hashCode()), "");//Initializing new row of hashmap with key value

                    Thread clientHandler = new Thread(new ClientHandler(clientSocket, printWriter));//new thread and sending new params for the runnable class
                    clientHandler.start();//jumping/starting the thread
                    System.out.print("New Connection: ");
                }
            } catch (Exception ex) {
                System.out.println("Error making a connection. Reason: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public static void userAdd(String name) {
        String message;
        final String add = ": :Connect";//constant string states
        users.add(name);//adding this user to the arraylist
        String[] tempList = new String[(users.size())];//creating new temporary string array
        users.toArray(tempList);//copying arraylist from users into templist string array

        for (String token : tempList) {
            message = (token + add);
            sendMessage(message);
        }
    }

    public static void userRemove(String data) {
        String message;
        final String add = ": :Connect";
        String name = data;
        users.remove(name);
        String[] tempList = new String[(users.size())];
        users.toArray(tempList);

        for (String token : tempList) {
            message = (token + add);
            sendMessage(message);
        }
    }

    public static void sendMessage(String message) {
        Iterator it = clientOutputStreams.iterator();//creating an iterator over a collection of clientoutputstream which has printwriter arraylist 

        while (it.hasNext()) {
            try {
                PrintWriter writer = (PrintWriter) it.next();//casting into printwriter row gathered from the iterator
                writer.println(message);//sending message through output
                writer.flush();//flushing all the buffers in a chain of Writers and OutputStreams.

            } catch (Exception ex) {
                System.out.println("Failed to send message for clients. \n");
            }
        }
    }

}
